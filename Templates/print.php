<?php
require_once('Library/Generator.php');
require_once('Library/Util.php');

$tables = getTables();
$table = isset($_GET['table']) ? $_GET['table'] : '';
$startDelimiter = '<pre>';
$endDelimiter = '</pre>';

function treatRow($table, $tables, $startDelimiter, $endDelimiter) {
    $statusText = '';
    $isFailed = false;
    $data = '';
    $output = '';

    if (($table != '' and in_array($table, $tables)) === false ) {
        $statusText = 'Table not found ';
        if($table !=='' ) {
            $statusText .= ': please add the file <strong>' . $table . '.csv</strong> in the Input folder for the table <strong>' . $table . '</strong>';
        }
        $statusText .= '<br/>Go back to the >> <a onclick="window.close()" style="font-weight: bold; cursor: pointer">table list</a> <<';
        $isFailed = true;
    }

    // Require the Mapper
    if((@include 'Mapper/'.$table.'_mapper.php') === false && $isFailed === false)
    {
        $statusText = 'Mapper not found ';
        if($table !=='' ) {
            $statusText .= ': please add the file <strong>' . $table . '_mapper.php</strong> in the Input folder for the table <strong>' . $table . '</strong>';
        }
        $statusText .= '<br/>Go back to the >> <a onclick="window.close()" style="font-weight: bold; cursor: pointer">table list</a> <<';
        $isFailed = true;
    }

    // Get the generated data fixtures and store it in $data variable
    if($isFailed === false) {
        eval('$data = generateTableFixtures('.ucfirst($table).'Mapper::map(), $startDelimiter, $endDelimiter);');
    }

    // Output => Success ( show the generated file ) => Failed ( Show an error text )
    if ($isFailed === true) {
        $output .= '<div class="alert alert-' . ($isFailed === true ? 'danger' : 'success') . '" role="alert">';
        $output .= $statusText;
        $output .= '</div>';
    } else {
        $output .= $data;
    }

    return  ['output' => $output, 'isFailed' => $isFailed];
}

$result = treatRow($table, $tables, $startDelimiter, $endDelimiter);

if($result['isFailed'] === true) {
    ob_start();
    echo '
    <style>
        .alert {
            max-width: 800px !important;
            margin: 50px auto;
        }
    </style>';
    $styles = ob_get_clean();
    // Content
    ob_start();
        echo $result['output'];
        $content = ob_get_clean();
    include 'layout.php';
} else {
    echo $result['output'];
}

?>