<?php
    require_once('Library/Util.php');
?>
<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="Assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="Assets/fontawesome/css/all.css"> <!--load all styles -->
        <?= $styles ?>
    </head>
    <body>
        <?= $content ?>
        <script src="Assets/js/jquery-3.3.1.slim.min.js"></script>
        <script src="Assets/js/popper.min.js"></script>
        <script src="Assets/js/bootstrap.min.js"></script>
    </body>
</html>
