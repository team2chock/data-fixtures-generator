<?php
require_once('Library/Util.php');
?>

<?php ob_start(); ?>
<style>
    .header {
        max-width: 800px;
        margin: 50px auto;
    }
    table {
        max-width: 800px !important;
        margin: 50px auto;
        border: 1px solid lightgrey;
    }
</style>
<?php $styles = ob_get_clean(); ?>

<?php ob_start(); ?>
<div class="header">
  <a href="?page=migrate-all" class="btn btn-info">
        Migrate all tables <i class="fas fa-sync"></i>
  </a>
</div>

<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">
                Table ( Input )
            </th>
            <th scope="col">
                Mapper
            </th>
            <th scope="col">
                Data Fixture ( Output )
            </th>
            <th scope="col">
                Actions
            </th>
        </tr>
    </thead>
    <tbody>
<?php $tables = getTables();

if(count($tables) > 0) {
    foreach ($tables as $table) {
        $previewLink = getUrl('?page=print&table=' . $table);
        $migrateLink = getUrl('?page=migrate&table=' . $table);
        $mapperFileName = 'Mapper/' . $table . '_mapper.php';
        $doMapperExist = file_exists($mapperFileName);
        $dataFixturesFileName = 'Output/' . $table.'.yml';
        $doFixturesFileExist = file_exists($dataFixturesFileName);
    ?>
        <tr>
            <td>
                Input/<?php echo $table ?>.csv
            </td>
            <td>
                <?php echo $doMapperExist ? "<span class=\"text-success\" >$mapperFileName</span>" : '<span class="text-danger" >No Mapper found</span>'; ?>
            </td>
            <td>
                <?php echo $doFixturesFileExist ? "<span class=\"text-success\" >$dataFixturesFileName</span>" : '<span class="text-danger" >No Datafixture found</span>'; ?>
            </td>
            <td>
                <a class="btn btn-primary" href="<?php echo $previewLink ?>" target="_blank">Preview</a> <a class="btn btn-success" href="<?php echo $migrateLink ?>" target="_blank">Migrate</a>
            </td>
        </tr>
<?php
    }
} else {
   ?>
    <tr>
        <td colspan="4">
            No table has been found
        </td>
    </tr>
   <?php
}

?>
    </tbody>
</table>

<?php $content = ob_get_clean() ?>

<?php include 'layout.php' ?>
