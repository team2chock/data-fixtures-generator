<?php
require_once('Library/Generator.php');
require_once('Library/Util.php');
?>

<?php ob_start(); ?>
<style>
    .header {
        max-width: 800px;
        margin: 50px auto;
    }
    table {
        max-width: 800px !important;
        margin: 50px auto;
        border: 1px solid lightgrey;
    }
</style>
<?php $styles = ob_get_clean(); ?>

<?php ob_start(); ?>
<div class="header">
    <a href="?page=tables" class="btn btn-info">
        <i class="fas fa-arrow-left"></i> Go back to all tables
    </a>
</div>

<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">
            Table ( Input )
        </th>
        <th scope="col">
            Status
        </th>
    </tr>
    </thead>
    <tbody>
<?php

$tables = getTables();
$startDelimiter = '';
$endDelimiter = "\n";

function treatRow($table, $tables) {
    $statusText = '';
    $isBlocked = false;

    if (($table != '' and in_array($table, $tables)) === false ) {
        $statusText = 'Table not found (no csv in Input folder) ';
        $isBlocked = true;
    }

    // Require the Mapper
    if((@include 'Mapper/'.$table.'_mapper.php') === false && $isBlocked === false)
    {
        $statusText = 'Mapper not found (Create the ' . $table . '_mapper.php in Mapper folder )';
        $isBlocked = true;
    }

    // Get the generated data fixtures and store it in $data variable
    if($isBlocked === false) {
        $data = '';
        eval('$data = generateTableFixtures('.ucfirst($table).'Mapper::map(), $startDelimiter, $endDelimiter);');

        // Save the generated data fixtures in a yml file
        $filePath = sprintf('./Output/%s.yml',$table);
        $fp = fopen($filePath, 'w');
        fwrite($fp, $data);
        fclose($fp);

        $statusText = "Migration OK ( generated file <strong>" . $filePath . "</strong> )";
    }

    if($isBlocked === true) {
        $output = '<span class="text-danger">';
        $output .= $statusText;
        $output .= '</span>';

    } else {
        $output = '<span class="text-success">';
        $output .= $statusText;
        $output .= '</span>';
    }

    return  "<tr><td>Input/$table.csv</td><td>$output</td></tr>";

}

foreach ($tables as $table) {
   echo treatRow($table, $tables);
}
?>
    </tbody>
</table>



<?php
$content = ob_get_clean() ?>

<?php include 'layout.php' ?>



