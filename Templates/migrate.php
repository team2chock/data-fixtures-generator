<?php
require_once('Library/Generator.php');
require_once('Library/Util.php');
?>

<?php ob_start(); ?>
<style>
    .alert {
        max-width: 800px !important;
        margin: 50px auto;
    }
</style>
<?php $styles = ob_get_clean(); ?>

<?php ob_start();

$tables = getTables();
$table = isset($_GET['table']) ? $_GET['table'] : '';
$startDelimiter = '';
$endDelimiter = "\n";

function treatRow($table, $tables) {
    $statusText = '';
    $isFailed = false;

    if (($table != '' and in_array($table, $tables)) === false ) {
        $statusText = 'Table not found ';
        if($table !=='' ) {
            $statusText .= ': please add the file <strong>' . $table . '.csv</strong> in the Input folder for the table <strong>' . $table . '</strong>';
        }
        $statusText .= '<br/>Go back to the >> <a href="?page=tables">table list</a> <<';
        $isFailed = true;
    }

    // Require the Mapper
    if((@include 'Mapper/'.$table.'_mapper.php') === false && $isFailed === false)
    {
        $statusText = 'Mapper not found ';
        if($table !=='' ) {
            $statusText .= ': please add the file <strong>' . $table . '_mapper.php</strong> in the Mapper folder for the table <strong>' . $table . '</strong>';
        }
        $statusText .= '<br/>Go back to the >> <a href="?page=tables">table list</a> <<';
        $isFailed = true;
    }

    // Get the generated data fixtures and store it in $data variable
    if($isFailed === false) {
        $data = '';
        eval('$data = generateTableFixtures('.ucfirst($table).'Mapper::map(), $startDelimiter, $endDelimiter);');

        // Save the generated data fixtures in a yml file
        $filePath = sprintf('./Output/%s.yml',$table);
        $fp = fopen($filePath, 'w');
        fwrite($fp, $data);
        fclose($fp);

        $statusText = sprintf('The table <strong>%s</strong> has been migrated and saved in the following path : <strong>%s</strong>', $table, $filePath) . '<br/><br/>';
        $statusText .= 'Go back to the >> <a href="?page=tables">table list</a> <<';
    }

    $output = '<div class="alert alert-' . ($isFailed === true ? 'danger' : 'success') . '" role="alert">';
    $output .= $statusText;
    $output .= '</div>';

    return  $output;
}

echo treatRow($table, $tables);
?>

<?php $content = ob_get_clean() ?>

<?php include 'layout.php' ?>



