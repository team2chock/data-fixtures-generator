## Initialize  function ( map )

The function map allows to declare some mandatory information about the table we want to migrate:

```php
public static function map(){  
    return Mapper::map(  
       >> CLASS_NAMESPACE <<,  
       >> UNIQUE_ID_COLUMN <<,  
       >> PARENT_TABLE_DATAFIXTURES <<,  
       >> TABLE_NAME <<,  
        self::getColumns()  
    );
}
```

|    NAME           | TYPE | DESCRIPTION                    |EXEMPLE OF VALUES                        |
|-------------------|------|-------------------------|-----------------------------|
|class namespace    | string |`namespace of the Entity`   |`'\AppBundle\Entity\Client\Client'`            |
|unique Id Column             | string|`unique id of the table`            |`'ID_CLIENT'`            |
|parent configuration file   | boolean or string|`if your table is inheritating another table, you can provide the name of the parent configuration file (yaml), otherwise provide false`| `src/AppBundle/Datafixtures/ORM/my_parent.yml`|
|table name      | string|`the name of the table to migrate (for example we will provide "client" for the table client.csv)`| `client`|
|self::getColumns()         |  - - - - - - |`Keep as it is (theses are the columns mappings)`|`self::getColumns()`|


## Mapper function ( getColumns )

The function getColumns describes how we want to migrate each column of the table:

```php
private static function getColumns(){  
    return [  
        ['new_name' => 'number', 'old_name' => 'ID_CLIENT'],  
        ['new_name' => 'name', 'old_name' => 'denom_sociale'],  
        /* Normal addresses */  
        ['new_name' => 'street', 'old_name' => 'adresse'],  
        ['new_name' => 'city', 'old_name' => 'ville'],  
        ['new_name' => 'zipCode', 'old_name' => 'cp'],  
        ['new_name' => 'phone', 'old_name' => 'telephone'],  
        ['new_name' => 'fax', 'old_name' => 'fax'],
        [... CODE IGNORED ...]
```

|    NAME           | TYPE |  MANDATORY | DESCRIPTION                    |EXEMPLE OF VALUES                        |
|-------------------|------|--------|-----------------|-----------------------------|
|old_name    | string | true | `name of the column in the former table`   |`'adresse'`            |
|new_name             | string | true |`name of the column in the new table`            |`'street'`            |
|mapping_function   | function| false | `you can apply a function to transform the data for the new table`|
|is_extra      | boolean | false |`this attribute allows to add column which does not exist in `| `client`|
|extra_value         |  string|boolean | false |`Keep as it is (theses are the columns mappings)`|`self::getColumns()`|