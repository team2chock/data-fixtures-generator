<?php
require_once('Library/Mapper.php');

class ClientMapper {
    public static function map(){
        return Mapper::map(
            '\AppBundle\Entity\Client\Client',
            'ID_CLIENT',
            false,
            'client',
            self::getColumns()
        );
    }

    private static function getColumns(){
        return [
            ['new_name' => 'number', 'old_name' => 'ID_CLIENT'],
            ['new_name' => 'name', 'old_name' => 'denom_sociale'],
            /* Normal addresses */
            ['new_name' => 'street', 'old_name' => 'adresse'],
            ['new_name' => 'city', 'old_name' => 'ville'],
            ['new_name' => 'zipCode', 'old_name' => 'cp'],
            ['new_name' => 'phone', 'old_name' => 'telephone'],
            ['new_name' => 'fax', 'old_name' => 'fax'],
            ['new_name' => 'email', 'old_name' => 'email'],
            /* Billing addresses */
            ['new_name' => 'billingStreet', 'old_name' => 'adresse'],
            ['new_name' => 'billingCity', 'old_name' => 'ville'],
            ['new_name' => 'billingZipCode', 'old_name' => 'cp'],
            ['new_name' => 'billingPhone', 'old_name' => 'telephone'],
            ['new_name' => 'billingFax', 'old_name' => 'fax'],
            ['new_name' => 'billingEmail', 'old_name' => 'email'],
            /* addresses end */
            ['new_name' => 'responsible', 'old_name' => 'responsable'],
            ['new_name' => 'responsiblePhone', 'old_name' => 'tel_responsable'],
            ['new_name' => 'responsibleEmail', 'old_name' => 'mail_responsable'],
            ['new_name' => 'entity', 'old_name' => 'id_entite', 'mapping_function' => function($data) { return $data == '1' ? 'entityOne' : 'entityTwo'; }],
            ['new_name' => 'enterprise', 'old_name' => 'mis_a_jour_sur_kizeo', 'is_extra' => true, 'extra_value' => '@enterprise_initial'],
            ['new_name' => 'postedBy', 'old_name' => 'mis_a_jour_sur_kizeo', 'is_extra' => true, 'extra_value' => '@user_bko_superadmin'],
        ];
    }
}
