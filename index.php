<?php

// Frontend Controller

$page = isset($_GET['page']) ? $_GET['page'] : 'tables';
$pagePath = sprintf('./Templates/%s.php', $page);

if((@include $pagePath) === false)
{
    echo 'page not found';
}
?>
