<?php
require_once('Extractor.php');

class Mapper {
    public static function map($namespace, $uniqueIdColumn, $extendedName, $table, $columns, $mappingFunction = false){
        $values = getData('Input/' . $table . '.csv');
        return [
            'class_namespace' => $namespace,
            'item_type' => $table,
            'unique_id_column' => $uniqueIdColumn,
            'extended_name' => $extendedName,
            'columns' => $columns,
            'values' => $values,
            'mapping_function' => $mappingFunction
        ];
    }
}
