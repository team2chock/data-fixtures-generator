<?php
require_once('Slugify.php');

function generateItemAttributes($data, $startDelimiter = "<pre>", $endDelimiter = "</pre>"){
    $output = '';
    $slugify = new Slugify();
    $dataColumns = $data['columns'];
    $dataValues = $data['values'];
    $itemTypeNameColumn = $data['item_type'];
    $uniqueIdColumn = $data['unique_id_column'];
    $mappingFunction = $data['mapping_function'];
    $extention = isset($data['extended_name']) && trim($data['extended_name']) != '' && $data['extended_name'] != false ? ' (extends ' .$data['extended_name'] . ')' : '';
    foreach ($dataValues as $value) {
        $endOfItemName = $mappingFunction != false && isset($mappingFunction['item_type']) ? $mappingFunction['item_type']($value[$uniqueIdColumn]) : $value[$uniqueIdColumn];
        $output .= "\n$startDelimiter    " . $slugify->slugify($itemTypeNameColumn, '_') .'_' . $slugify->slugify($endOfItemName, '_') . $extention .":$endDelimiter";
        foreach ($dataColumns as $column) {
            if(isset($column['is_extra']) && $column['is_extra'] == true){
                $nameToDiplay = $column['new_name'];
                $valueToDisplay = $column['extra_value'];
            } else {
                $nameToDiplay = isset($column['new_name']) ? $column['new_name'] : 'not_found';
                $valueToDisplay = isset($column['old_name']) && isset($value[$column['old_name']]) ? (isset($column['mapping_function']) ? ($column['mapping_function']($value[$column['old_name']], $value)) : $value[$column['old_name']]) : 'not_found';
            }

            // Detect the type
            $isDatetime = isset($column['type']) && $column['type'] == 'Datetime';
            $isArray = isset($column['type']) && $column['type'] == 'Array';
            $isString = !isset($column['type']) || (isset($column['type']) && $column['type'] == 'String');
            $isMultilineText = isset($column['type']) && $column['type'] == 'Multiline';

            // Display differnt way depending on the type
            $output .= checkDatetimeData($isDatetime, $nameToDiplay, $valueToDisplay, $startDelimiter, $endDelimiter);

            $output .= checkStringData($isString, $nameToDiplay, $valueToDisplay, $startDelimiter, $endDelimiter);

            $output .= checkArrayData($isArray, $nameToDiplay, $valueToDisplay, $startDelimiter, $endDelimiter);

            $output .= checkMultilineData($isMultilineText, $nameToDiplay, $valueToDisplay, $startDelimiter, $endDelimiter);
        }
    }
    return $output;
}

function checkArrayData($isArray, $name, $value, $startDelimiter = "<pre>", $endDelimiter = "</pre>"){
    if($isArray){
        return "\n$startDelimiter        " . $name . ':                  ' . $value . "$endDelimiter";
    }
    return '';
}

function checkStringData($isString, $name, $value, $startDelimiter = "<pre>", $endDelimiter = "</pre>"){
    if($isString){
        $value = str_replace('\"', '”', $value);
        $value = str_replace('"', '”', $value);
        $value = str_replace('<', '&lt;', $value);
        $value = str_replace('&quot;', '”', $value);
        $value = str_replace("\\", "\\\\", $value);
        $value = $value != NULL && trim($value) != '' ? '"' . $value . '"' : 'NULL';
        return "\n$startDelimiter        " . $name . ':                  ' . $value . "$endDelimiter";
    }
    return '';
}

function checkDatetimeData($isDatetime, $name, $value, $startDelimiter = "<pre>", $endDelimiter = "</pre>"){
    if($isDatetime){
        $isNull = strtolower($value) == 'null' || strtolower($value) == '' || strtolower($value) == '0000-00-00' || strtolower($value) == '0000-00-00 00:00:00';
        $finalValue = $isDatetime ? (!$isNull ? '&lt;identity(new \DateTime("' . $value . '"))&gt;' : 'NULL') : ('"' . $value . '"');
        return "\n$startDelimiter        " . $name . ':                  ' . $finalValue . "$endDelimiter";
    }
    return '';
}

function checkMultilineData($isMultiline, $name, $value, $startDelimiter = "<pre>", $endDelimiter = "</pre>"){
    $output = '';
    $value = str_replace('\"', '”', $value);
    $value = str_replace('"', '”', $value);
    $value = str_replace('<', '&lt;', $value);
    $value = str_replace('&quot;', '”', $value);
    $value = str_replace("\\", "\\\\", $value);
    if($isMultiline){
        $lineData = '';
        $lines = preg_split("/((\r?\n)|(\r\n?))/", $value);
        $lineNumber = count($lines);
        if($lineNumber > 1){
            foreach($lines as $key => $line){
                $lineData = $lineData . ($startDelimiter == "<pre>" ? ("$startDelimiter        "):("$startDelimiter" )) . $line;

                if($key == ($lineNumber - 1)){
                    $lineData = $lineData . "\"$endDelimiter";
                } else {
                    $lineData = $lineData . "\n$endDelimiter";
                }
            }
            $lineData = "$endDelimiter" . $lineData;
        } else {
            $lineData = $lineData . "\"$endDelimiter";
        }
        $output .= ($startDelimiter == '<pre>' ? '<pre>        ' : "$startDelimiter") . $name . ': "';
        $output .= $lineData;
    }
    return $output;
}

function generateTableFixtures($data, $startDelimiter = "<pre>", $endDelimiter = "</pre>"){
    $output = '';
    $classNameSpace = isset($data['class_namespace']) ? $data['class_namespace'] : 'not_found';
    $itemTypeName = isset($data['item_type']) ? $data['item_type'] : 'item';
    $uniqueIdColumn = isset($data['unique_id_column']) ? $data['unique_id_column'] : '';
    $extention = isset($data['extended_name']) && trim($data['extended_name']) != '' && $data['extended_name'] != false ? ' (extends ' .$data['extended_name'] . ')' : '';
    if($extention != ''){
        $output .= "$startDelimiter"."include:$endDelimiter";
        $output .= '$startDelimiter    - ' . $data['extended_name'] . ".yml$endDelimiter";
        $output .= ($endDelimiter == "<pre>" ? "<br/>" : "$endDelimiter");
    }
    $output .= "$startDelimiter" . $classNameSpace . ":$endDelimiter";
    $output .= generateItemAttributes($data, $startDelimiter, $endDelimiter);
    return $output;
}
