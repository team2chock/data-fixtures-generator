<?php

function getData($filePath)
{
    $handle = @fopen($filePath, 'r');
    $data = $fields = array(); $i = 0;

    if ($handle) {
        while (($row = fgetcsv($handle, 4096)) !== false) {
            if (empty($fields)) {
                $fields = $row;
                continue;
            }
            foreach ($row as $k=>$value) {
                $data[$i][$fields[$k]] = $value;
            }
            $i++;
        }
        if (!feof($handle)) {
            echo 'Error: unexpected fgets() fail\n';
        }
        fclose($handle);
    }

    return $data;
}
