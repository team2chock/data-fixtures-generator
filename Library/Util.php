<?php

function getUrl($path){
    return sprintf(/*
        "%s://%s:%s/%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME'],
        $_SERVER['SERVER_PORT'],*/
    '/%s',
        $path
    );
}

function getTables() {
    return array_map(function($fileName){
        return str_replace('.csv', '',
            str_replace( 'Input/', '', $fileName)
        );
    }, glob('Input/*.csv'));
}
