Data Fixture Generator v1.0.0
=============================

The "Data Fixture Generator" application has been created with the following goals :  
- Migrate data from a A database to a B database and adapt them to the new schema


Requirements
------------

  * [Docker](https://docs.docker.com/engine/install/);
  * [Docker-compose](https://docs.docker.com/compose/install/);

The "Data Fixtures Generator" application is made with php 7+ 

Installation
------------ 

Execute the below commands which will do the following actions :  

- Navigate to the project folder via the terminal
- Generate the environment variables required by the generated parameters.yml file

```bash
$ cd data-fixtures-generator/
$ docker-compose -f docker-compose__nginx__php.yml build
$ docker-compose -f docker-compose__nginx__php.yml up -d
``` 
then open the following URL [http://localhost:8011](http://localhost:8011)

Usage
------------ 

This project allows you to migrate databases very simply  

1 - Put your tables exported in csv format in the folder **Input**

2 - Write a Mapper class for your table to migrate by respecting this naming convention :  
**table_name**_mapper.php.  

For example :  

  * toto.csv => toto_mapper.php
  * client.csv => client_mapper.php
  * my_other_table.csv => my_other_table_mapper.php
  
open this file to see how to write a Mapper => [Doc/usage.md](Doc/usage.md) 
  
3 - Open the http://localhost:8011 and click on "migrate all tables"

4 - The generated file is available in the **Output** folder


Troubleshooting
---------------

```
